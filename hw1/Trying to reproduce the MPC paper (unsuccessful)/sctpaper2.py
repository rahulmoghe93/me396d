# Generate data for control problem.
import numpy as np
import scipy.linalg as alg
from sympy.abc import t, s
from sympy import Matrix
from sympy.integrals.transforms import inverse_laplace_transform
# import matplotlib.pyplot as plt
from cvxpy import *
# Parameters describing the 
tau2 = 10.0
tau3 = 30.0
tau4 = 0.8
tau5 = 2.0
tau6 = 0.5
g311 = 0.4
g29 = 0.4
g510 = 0.6
g57 = 1.0
g64 = 1.5
g68 = 0.0
b25 = 0.5
b34 = 0.2
b42 = 0.3
b43 = 0.9
b45 = 0.5
b46 = 0.9
b54 = 0.6
nu8 = 6
nu9 = 5
A = np.array([[-1/tau2,0.0,0.0,b25/tau2,0.0],
	[0.0,-1/tau3,b34/tau3,0.0,0.0],
	[b42/tau4,b43/tau4,-1/tau4,b45/tau4,b46/tau4],
	[0.0,0.0,b54/tau5,-1/tau5,0.0],
	[0.0,0.0,0.0,0.0,-1/tau6]])
Ad = Matrix(s*np.identity(5) - A)
Ad = Ad.inv()
# Adi = inverse_laplace_transform(Ad, s ,t)

Ad = A + np.identity(5)
B = np.array([[0.0,0.0,0.0,g29/tau2,0.0,0.0],
	[0.0,0.0,0.0,0.0,0.0,g311/tau3],
	[0.0,0.0,0.0,0.0,0.0,0.0],
	[0.0,g57/tau5,0.0,0.0,g510/tau5,0.0],
	[g64/tau6,0.0,g68/tau6,0.0,0.0,0.0],])
a = np.array([B[:,0]])
b = np.array([B[:,2:4]])
Bnew = np.concatenate((a.T,b.reshape(5,2)), axis=1)
Bd = B[:,1]
Bd = Bd[:,np.newaxis]
 
nx = 5						# Dimension of the state space
nu = 3						# Dimension of the input space
ndelta = 1+nu9+nu8			# Dimension of
nz = ndelta					# Dimension of the discrete input space
ny = 4						# Dimension of the output
U89 = np.transpose(np.array([5000.0,6000.0,7000.0,8000.0,9000.0,10000.0,100.0,200.0,300.0,400.0,500.0]))
U8 = np.transpose(np.array([5000.0,6000.0,7000.0,8000.0,9000.0,10000.0]))
U9 = np.transpose(np.array([100.0,200.0,300.0,400.0,500.0]))
p = 7						# prediction horizon
m = 5						# control move horizon
dt = 1						# T iteration in days
u_min = np.array([[5000.0],[0.0],[0.0]])
u_max = np.array([[10000],[500.0],[500.0]])
du_max = np.array([[1000],[500.0],[500.0]])
du_min = -du_max
y_min = np.array([[0.0],[0.0],[0.0],[0.0]])
y_max = np.array([[10000.0],[10000.0],[12000.0],[10000.0]])
d_prime = 20*np.random.randn(1,1)
Qy = np.zeros((ny,ny))
Qy[3,3] = 1.0
y_r = np.array([[0.0],[0.0],[0.0],[10000.0]])

def d(k):
	if k >= 110.0 and k <= 125.0:
		return 20.0
	else:
		return 80.0

k = 1
# print Bd.shape
# print Ad.shape
# print Bnew.shape
# print B.shape

x0 = np.array([[0.0],[0.0],[5000.0],[0.0],[0.0]])
u0 = np.array([[5000.0],[0.0],[0.0]])
print x0.shape
while k < 3:
	x = Variable(nx,p+1)
	u = Variable(nu,p)
	y = Variable(ny,p)
	z = Variable(nz,p)
	delta = Bool(ndelta,p)
	d_prime = np.random.randn(1,p)
	# print diag(delta[1:,0])*U89
	states = []
	for t in range(p):
		cost = sum_squares(y[:,t] - y_r)
		constr = [
		x[:,t+1] == Ad*x[:,t] + Bnew*u[:,t] + Bd*d(t),
					y[:,t] == x[0:4,t],
					# sum_entries(delta[1:,t]) >= 1,z[1:,t] == diag(delta[1:,t])*U89,
					y[2,t] - u[0,t] <= delta[0,t]*(y_max[2] - u_min[0]),
					y[2,t] - u[0,t] >= (1 - delta[0,t])*(y_min[2] - u_max[0]),
					u[1,t] - z[0,t] <= (1 - delta[0,t])*(u_max[1] - u_min[2]),
					u[1,t] - z[0,t] >= (1 - delta[0,t])*(u_min[1] - u_max[2]),
					z[0,t] >= delta[0,t]*u_min[2], z[0,t] <= delta[0,t]*u_max[2],
					y_min <= y[:,t], y[:,t] <= y_max]
	for m in range(m):
		constr += [u_min <= u[:,m], u[:,m] <= u_max,
					du_min <= u[:,m+1] - u[:,m], u[:,m+1] - u[:,m] <= du_max]
		if t == 0:
			constr += [x[:,0] == x0, u[:,0] == u0]
		states.append(Problem(Minimize(cost), constr))
	prob = sum(states)
	result = prob.solve(verbose=False)
	print 'Solver status: ', prob.status
	# print delta.value
	for i in range(3):
		x0[i] = x[i,1].value
		u0[i] = u[i,1].value
	x0[3] = x[3,1].value
	x0[4] = x[4,1].value
	k += 1

"""np.random.seed(1)
n = 8
m = 2
T = 50
alpha = 0.2
beta = 5
A = np.eye(n) + alpha*np.random.randn(n,n)# Generate data for control problem.
import numpy as np
np.random.seed(1)
n = 8
m = 2
T = 50
alpha = 0.2
beta = 5
A = np.eye(n) + alpha*np.random.randn(n,n)
B = np.random.randn(n,m)
x_0 = beta*np.random.randn(n,1)

# Form and solve control problem.
from cvxpy import *
x = Variable(n, T+1)
u = Variable(m, T)

states = []
for t in range(T):
    cost = sum_squares(x[:,t+1]) + sum_squares(u[:,t])
    constr = [x[:,t+1] == A*x[:,t] + B*u[:,t],
              norm(u[:,t], 'inf') <= 1]
    states.append( Problem(Minimize(cost), constr) )
# sums problem objectives and concatenates constraints.
prob = sum(states)
prob.constraints += [x[:,T] == 0, x[:,0] == x_0]
prob.solve()

# Plot results.

f = plt.figure()

# Plot (u_t)_1.
ax = f.add_subplot(411)
plt.plot(u[0,:].value.A.flatten())
plt.ylabel("$(u_t)_1$")
plt.yticks(np.linspace(-1.0, 1.0, 3))
plt.xticks([])

# Plot (u_t)_2.
plt.subplot(4,1,2)
plt.plot(u[1,:].value.A.flatten())
plt.ylabel("$(u_t)_2$")
plt.yticks(np.linspace(-1, 1, 3))
plt.xticks([])

# Plot (x_t)_1.
plt.subplot(4,1,3)
x1 = x[0,:].value.A.flatten()
plt.plot(x1)
plt.ylabel("$(x_t)_1$")
plt.yticks([-10, 0, 10])
plt.ylim([-10, 10])
plt.xticks([])

# Plot (x_t)_2.
plt.subplot(4,1,4)
x2 = x[1,:].value.A.flatten()
plt.plot(range(51), x2)
plt.yticks([-25, 0, 25])
plt.ylim([-25, 25])
plt.ylabel("$(x_t)_2$")
plt.xlabel("$t$")
plt.tight_layout()
plt.show()
B = np.random.randn(n,m)
x_0 = beta*np.random.randn(n,1)

# Form and solve control problem.
from cvxpy import *
x = Variable(n, T+1)
u = Variable(m, T)

states = []
for t in range(T):
    cost = sum_squares(x[:,t+1]) + sum_squares(u[:,t])
    constr = [x[:,t+1] == A*x[:,t] + B*u[:,t],
              norm(u[:,t], 'inf') <= 1]
    states.append( Problem(Minimize(cost), constr) )
# sums problem objectives and concatenates constraints.
prob = sum(states)
prob.constraints += [x[:,T] == 0, x[:,0] == x_0]
prob.solve()

# Plot results.
import matplotlib.pyplot as plt

f = plt.figure()

# Plot (u_t)_1.
ax = f.add_subplot(411)
plt.plot(u[0,:].value.A.flatten())
plt.ylabel("$(u_t)_1$")
plt.yticks(np.linspace(-1.0, 1.0, 3))
plt.xticks([])

# Plot (u_t)_2.
plt.subplot(4,1,2)
plt.plot(u[1,:].value.A.flatten())
plt.ylabel("$(u_t)_2$")
plt.yticks(np.linspace(-1, 1, 3))
plt.xticks([])

# Plot (x_t)_1.
plt.subplot(4,1,3)
x1 = x[0,:].value.A.flatten()
plt.plot(x1)
plt.ylabel("$(x_t)_1$")
plt.yticks([-10, 0, 10])
plt.ylim([-10, 10])
plt.xticks([])

# Plot (x_t)_2.
plt.subplot(4,1,4)
x2 = x[1,:].value.A.flatten()
plt.plot(range(51), x2)
plt.yticks([-25, 0, 25])
plt.ylim([-25, 25])
plt.ylabel("$(x_t)_2$")
plt.xlabel("$t$")
plt.tight_layout()
plt.show()
"""