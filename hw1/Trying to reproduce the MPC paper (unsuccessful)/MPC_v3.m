clc;
close all;
clear all;

%Parameter definition
load('MPC_param.mat'); %Loads Beta, Gamma, Tau matrices
Ksr = 0.8;
beta46 = 0.9;
gamma64 = 1.5;
nx = numel(tau); %Number of state variables
lambda = 1;
delta = 1; %Sampling time

%Discretize model
Tau = diag(tau);
A = Tau\(beta - eye(nx));
B = Tau\gamma;
C = eye(nx);
D = zeros(nx,11);
SYS = ss(A,B,C,D);
SYSD = c2d(SYS, delta,'zoh');
A_d = SYSD.A;
B_d = SYSD.B;

%Set matrices
ny = 4;
Cx = [eye(ny) zeros(4,1)];
Bxi = [B_d(:,4) B_d(:,9:11)];
ku = [eye(3); -1 0 0];
kn = [-1 0 0 1]'*[0 0 1 0 0];
Ax = (A_d + Bxi*kn);
Bu = Bxi*ku;
Bd = B_d(:,7);
U8 = [5000 6000 7000 8000 9000 10000]';
U9 = [100 200 300 400 500]';
D = diag([U8; U9]);
nz = numel(U8) + numel(U9);
nzbar = nz+1;
nu = size(Bu,2);
nu8 = numel(U8);
nu9 = numel(U9);
nd = size(Bd,2);
H = [ones(1,nu8)  zeros(1,nu9);
     zeros(1,nu8) ones(1,nu9)];
Hbar = blkdiag(H,1);

%Start the problem
p = 70;
U = sdpvar(nu*p,1);
Y4 = sdpvar(p,1);
d = 80*ones(1,p);
Eta0 = [0 0 5000 0 0]';
ref4 = 10000;
  
%Constrain the problem to follow dynamics
ky = [0 0 1 0];
kf = p;
B_kf = State_Tran_Mat_tf(kf,Ax,Bu);
Bd_kf = State_Tran_Mat_tf(kf,Ax,Bd);
A_u = []; 
b_u = [];
for i = 1:p
    [A_k, B_k] = State_Tran_Mat_k(i,kf,Ax, B_kf);
    [A_k, Bd_k] = State_Tran_Mat_k(i,kf,Ax, Bd_kf);
    A_u = [A_u; ky*Cx*B_k];
    b_u = [b_u; ky*Cx*(Ax*Eta0 + Bd_k*d')];
end
Eqs = [];
Eqs = [Eqs;
       Y4 == A_u*U + b_u];
   
%Constrain U8 and U9 to be discrete values
z = sdpvar(nz*p,1);
delta = sdpvar(nz*p,1);
U89 = [eye(2) zeros(2,1)];
deltaU = [eye(nu) -eye(nu)];
U89_big = [];
H_big = [];
D_big = [];
deltaU_big = [];
for i = 1:p
    H_big = blkdiag(H_big,H);
    D_big = blkdiag(D_big,D);
    U89_big = blkdiag(U89_big,U89);
    if(i<p)
        deltaU_big = [deltaU_big zeros(size(deltaU_big,1),nu);
                      zeros(nu,size(deltaU_big,2)-nu) deltaU];
    end
end
Eqs = [Eqs;
       H_big*delta == ones(2*p,1);
       z == D_big*delta;
       U89_big*U == H_big*z];

%Inequalities
Ineqs = [];
umin = [5000 0 0]';
umax = [10000 500 500]';
deltaUmin = [-1000 -500 -500]';
deltaUmax = [1000 500 500]';
deltaMin = zeros(nz,1);
deltaMax = ones(nz,1);

Ineqs = [Ineqs;
        U >= repmat(umin,[p 1]);
        U <= repmat(umax,[p 1]);
        delta >= repmat(deltaMin, [p 1]);
        delta <= repmat(deltaMax, [p 1]);
        deltaU_big*U >= repmat(deltaUmin, [p-1 1]);
        deltaU_big*U <= repmat(deltaUmax, [p-1 1])];
    
%Set initial input
Aeq = [eye(3) zeros(nu,nu*(p-1))];
u0 = [5000 100 0]';
Eqs = [Eqs;
       Aeq*U == u0];

%Constrain input u9
wu9 = 0.003;
m = 5;
U9 = [0 1 0];
Ucons = [];
for i = 1:p
    if (i <= m)
        Ucons = blkdiag(Ucons, U9);
    else
        Ucons = blkdiag(Ucons, [0 0 0]);
    end
end
     
     
Constraints = [Eqs; Ineqs];
func_y = (Y4-ref4*ones(p,1))'*(Y4-ref4*ones(p,1));
func_u = (Ucons*U)'*(Ucons*U);
func = func_y;
ops = sdpsettings('solver','quadprog','quadprog.Algorithm','interior-point-convex','quadprog.Diagnostics','on','quadprog.MaxIter',1e4);
sol = optimize(Constraints,func,ops);
sol.info

U = double(U);

eta(:,1) = Eta0;
y4(1) = ky*Cx*eta(:,1);
for i = 1:p
    [A_k B_k] = State_Tran_Mat_k(i,kf,Ax, B_kf);
    [A_k Bd_k] = State_Tran_Mat_k(i,kf,Ax, Bd_kf);
    eta(:,i+1) = Ax*Eta0 + B_k*U + Bd_k*d';
    u(:,i) = U((i-1)*nu+1:i*nu);
    y4(i+1) = ky*Cx*eta(:,i+1);
end

fig = figure()
subplot(4,1,1); plot(eta(1,:)); xlabel('Days'); ylabel('\eta_2');
subplot(4,1,2); plot(eta(2,:)); xlabel('Days'); ylabel('\eta_3');
subplot(4,1,3); plot(eta(3,:)); xlabel('Days'); ylabel('\eta_4');
subplot(4,1,4); plot(eta(4,:)); xlabel('Days'); ylabel('\eta_5');

fig2 =figure()
subplot(3,1,1); plot(u(1,:)); xlabel('Days'); ylabel('u_8');
subplot(3,1,2); plot(u(2,:)); xlabel('Days'); ylabel('u_9');
subplot(3,1,3); plot(u(3,:)); xlabel('Days'); ylabel('u_{10}');

fig3 = figure(); 
plot(y4); xlabel('Days'); ylabel('\eta_4');