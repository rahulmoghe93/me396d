function B_kf = State_Tran_Mat_tf(kf,Ad,Bd)

% A_k = Power_A(kf,Ts);
nu = size(Bd,2);

for j = 0:kf-1
    B_kf(:,j*nu+1:(j+1)*nu) = Ad^(kf-1-j)*Bd;
end
