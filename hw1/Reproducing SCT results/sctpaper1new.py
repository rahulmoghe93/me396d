# Made by Rahul Moghe on 13th September 2016
# For the Course - Decision and Control of Human Centred Robots
# Simulation for dynamical system formulation of the system in
# Social Cognitive Theory Paper named " A Dynamical Systems Model of Social Cognitive Theory"

from scipy.integrate import ode
import numpy as np
import matplotlib.pyplot as plt

# Initial Conditions
eta0 = np.array([0.0,0.0,0.0,0.0,0.0,0.0])
eta0 = eta0.T

ksi0 = np.array([0.0,3.0,3.0,0.0,10.0,0.0,0.0,0.0])
scenario = 3

# Parameters describing scenario 1
tau = np.array([1.0,1.0,1.0,2.0,1.0,3.0])
g11 = 3.0
g13 = 1.0
g22 = 1.0
g24 = 1.1
g26 = 0.44
g28 = 0.5
g32 = 2.0
g33 = 1.0
g35 = 1.0
g36 = 1.0
g38 = 0.9
g57 = 2.0
g64 = 15.0
g68 = 15.0
b21 = 0.3
b31 = 0.5
b42 = 0.3
b43 = 0.8
b54 = 0.3
b34 = 0.2
b25 = 0.3
b14 = 0.23
b46 = 0.44
b45 = 0.1

# Input Function
def ksi(t):
	psy = np.zeros((8,1))
	if t>=1.0 and t<=6.0:
	    psy[7] = 6.0
	elif t>=10.0 and t<=12.0:
	    psy[7] = -1.0
	psy[1] = 5.0
	psy[2] = 5.0
	psy[4] = 10.0
	return psy
print eta0[1]

# The derivative function y_dot = fnew(t,y)
def f(t,y):
	grad = np.array([0.0,0.0,0.0,0.0,0.0,0.0])
	grad[0] = (g13*ksi0[2] + g11*ksi0[0] + b14*y[3] - y[0])/tau[0]
	grad[1] = (g22*ksi0[1] + g28*ksi0[7] + g24*ksi0[3] + g26*ksi0[5] + b21*y[0] + b25*y[4] - y[1])/tau[1]
	grad[2] = (g33*ksi0[2] + g38*ksi0[7] + b31*y[0] + b34*y[3] - y[2])/tau[2]
	grad[3] = (b42*y[1] + b43*y[2] + b46*y[5] + b45*y[4] - y[3])/tau[3]
	grad[4] = (g57*ksi0[6] + b54*y[3] - y[4])/tau[4]
	grad[5] = (g64*ksi0[3] + g68*ksi0[7] - y[5])/tau[5]
	return grad

# Integrate with zero initial conditions to find the actual initial conditions
r = ode(f).set_integrator('vode', method='bdf')

t0 = 0.0
tf = 100.0
dt = 0.01

n = np.floor((tf-t0)/dt) + 1

r.set_initial_value(eta0, t0)
t1 = np.zeros((n, 1))
y_t = np.zeros((6,n))

k = 1
while r.successful() and k < n:
	r.integrate(r.t + dt)

	t1[k] = r.t
	y_t[:,k] = r.y
	k += 1

# Get the steady state values for the initial condition
eta0 = y_t[:,-1]

# Define new function
def fnew(t,y):
	grad = np.zeros((6,1))
	grad[0] = (g13*ksi(t)[2] + g11*ksi(t)[0] + b14*y[3] - y[0])/tau[0]
	grad[1] = (g22*ksi(t)[1] + g28*ksi(t)[7] + g24*ksi(t)[3] + g26*ksi(t)[5] + b21*y[0] + b25*y[4] - y[1])/tau[1]
	grad[2] = (g33*ksi(t)[2] + g38*ksi(t)[7] + b31*y[0] + b34*y[3] - y[2])/tau[2]
	grad[3] = (b42*y[1] + b43*y[2] + b46*y[5] + b45*y[4] - y[3])/tau[3]
	grad[4] = (g57*ksi(t)[6] + b54*y[3] - y[4])/tau[4]
	grad[5] = (g64*ksi(t)[3] + g68*ksi(t)[7] - y[5])/tau[5]
	return grad

# Integrate again with the steady state values as the initial conditions
p = ode(fnew).set_integrator('vode', method='bdf')
t0 = 0.0
tf = 20.0
dt = 0.01

n = np.floor((tf-t0)/dt) + 1

p.set_initial_value(eta0, t0)
t2 = np.zeros((n,1))
y2 = np.zeros((6,n))

k = 1
while p.successful() and k < n:
	p.integrate(p.t + dt)

	t2[k] = p.t
	y2[:,k] = p.y
	k += 1

# Plotting all the states space evolution in 6 plots
fig = plt.figure(1)
fig.suptitle('SCT : Scenario {0}'.format(scenario),fontsize=16,fontweight='bold')
for i in range(1,7):
	plt.subplot(230+i)
	plt.plot(t2,y2[i-1,:].T, label=r'$\eta_{0} (t)$'.format(i))
	plt.xlabel('Time [days]')
	plt.ylabel(r'$\eta_{0}$'.format(i))
	plt.grid('on')
	plt.legend()
	plt.xlim([0,20])
	plt.ylim([1,100])
plt.show()