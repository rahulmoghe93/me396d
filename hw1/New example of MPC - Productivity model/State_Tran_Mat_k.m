function [A_k B_k] = State_Tran_Mat_k(k,kf,A_d, B_kf)

nx = size(A_d,1);
nu = size(B_kf,2)/kf;
B_k = zeros(size(B_kf));

if (k == 0)
    A_k = eye(nx);
else
    A_k = A_d^k;
    B_k(:,1:nu*k) = B_kf(:,end-nu*k+1:end);
end

% Ak_bar = [A_k B_k];