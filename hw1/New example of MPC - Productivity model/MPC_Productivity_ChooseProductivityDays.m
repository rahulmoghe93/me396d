clc;
clear all;
close all;

%MPC
load('Model.mat');
nx = size(A,1);
nu = size(B,2);
delta = 1; %Discretization period

%Discretize model
C = eye(nx);
D = zeros(nx,nu);
SYS = ss(A,B,C,D);
SYSD = c2d(SYS, delta,'zoh');
A_d = SYSD.A;
B_d = SYSD.B;

%Problem matrices
Ax = A_d;
Bu = B_d(:,1);
Bd = B_d(:,2);
nu = size(Bu,2);
nd = size(Bd,2);
Uset = 0:1:20;
nz = numel(Uset);
H = ones(1,nz);

%Start the problem
p = 10; %Predictive horizon
dmax = 3;
d_est = dmax*ones(p,1);
X0 = [5 0 0 0 5 0 0 0]';

%Peak productivity days
prod_days = [10 20]';
productivity = 15;

day = 0;

x(:,1) = X0;
final_day = 30;
for j = 1:final_day

%Constrain the problem to follow dynamics
U = sdpvar(nu*p,1);
X = sdpvar(nx*p,1);
kf = p;
Bu_kf = State_Tran_Mat_tf(kf,Ax,Bu);
Bd_kf = State_Tran_Mat_tf(kf,Ax,Bd);
A_u = [];
b_u = [];
for i = 1:p
    [A_k, Bu_k] = State_Tran_Mat_k(i,kf,Ax, Bu_kf);
    [A_k, Bd_k] = State_Tran_Mat_k(i,kf,Ax, Bd_kf);
    A_u = [A_u; Bu_k];
    b_u = [b_u; (A_k*X0 + Bd_k*d_est)];
end
Eqs = [];
Eqs = [Eqs;
       X == A_u*U + b_u];
   
%Vectors for state constraints
X5vec = [0 0 0 0 1 0 0 0];
X7vec = [0 0 0 0 0 0 1 0];
deltaU = [eye(nu) -eye(nu)];
V5_vecBig = [];
V7_vecBig = [];
deltaU_big = [];
for i = 1:p
    V5_vecBig = blkdiag(V5_vecBig,X5vec);
    V7_vecBig = blkdiag(V7_vecBig,X7vec);
    if(i<p)
        deltaU_big = [deltaU_big zeros(size(deltaU_big,1),nu);
                      zeros(nu,size(deltaU_big,2)-nu) deltaU];
    end
end

%Constrain input to be discrete values
z = sdpvar(nz*p,1);
delta = binvar(nz*p,1);
H_big = [];
D_big = [];
for i = 1:p
    H_big = blkdiag(H_big,H);
    D_big = blkdiag(D_big,diag(Uset));
end
Eqs = [Eqs;
       H_big*delta == ones(p,1);
       z == D_big*delta;
       U == H_big*z];

%Inequalities
Ineqs = [];
umin = 0;       umax = 20;
deltaUmin = -4; deltaUmax = 4;
x7max = 10;

Ineqs = [Ineqs;
        U >= repmat(umin,[p 1]);
        U <= repmat(umax,[p 1]);
        deltaU_big*U >= repmat(deltaUmin, [p-1 1]);
        deltaU_big*U <= repmat(deltaUmax, [p-1 1]);
        V5_vecBig*X >= 5*ones(p,1)];

%Peak productivity at day 10 and 20
A_u = [];
b_u = [];
days = [];
Xvec = [];
for i=1:numel(prod_days)
    if((prod_days(i) - day) > 0)
        days = [days; prod_days(i)-day];
    end
end
if(numel(days) > 0)
    for i = 1:numel(days)
        if(days(i) < p)
            [A_k, Bu_k] = State_Tran_Mat_k(days(i),kf,Ax, Bu_kf);
            [A_k, Bd_k] = State_Tran_Mat_k(days(i),kf,Ax, Bd_kf);
            A_u = [A_u; Bu_k];
            b_u = [b_u; (A_k*X0 + Bd_k*d_est)];
            Xvec = blkdiag(Xvec,X5vec);
        end
    end
    if(numel(Xvec) > 0)
        Ineqs = [Ineqs;
                 Xvec*(A_u*U + b_u) >= productivity*ones(size(Xvec,1))];
    end
end

        
%Solve problem
Constraints = [Eqs; Ineqs];
func_x5 = (V5_vecBig*X - 20*ones(p,1))'*(V5_vecBig*X - 20*ones(p,1));
% func_delta = (H_big*delta - ones(p,1))'*(H_big*delta - ones(p,1));
func_x7 = (V7_vecBig*X)'*(V7_vecBig*X);
func_u = (U)'*(U);
func = func_x7;
ops = sdpsettings('solver','gurobi');
sol = optimize([Constraints],func,ops);
sol.info

U = double(U);
X = double(X);
% reshape(X,[nx p])

u(:,j) = U(1:nu);
d(:,j) = dmax*rand(nd,1);
x(:,j+1) = Ax*X0 + Bu*u(:,j) + Bd*d(:,j);
X0 = x(:,j+1);
day = day + 1;

end


time = 0:1:day;
fig = figure(1);
subplot(4,1,1); plot(time,x(1,:)); 
xlabel('Days'); ylabel('Motivation'); ylim([0 20]);
subplot(4,1,2); plot(time,x(3,:)); 
xlabel('Days'); ylabel('Discouragement');
subplot(4,1,3); plot(time,x(5,:),[0 final_day],[5 5],'r--',[0 final_day],[15 15],'r--'); 
xlabel('Days'); ylabel('Productivity'); ylim([0 20]);
subplot(4,1,4); plot(time,x(7,:)); 
xlabel('Days'); ylabel('Stress'); ylim([0 15]);

fig = figure(2);
subplot(2,1,1); plot(time(1:end-1),u(1,:)); 
xlabel('Days'); ylabel('Motivational Input');  ylim([0 20]);
subplot(2,1,2); plot(time(1:end-1),d(1,:)); 
xlabel('Days'); ylabel('Discouragement Input');  ylim([0 3]);