function [time,Xguess,Uguess,feas] = solveSQP(Xg,Ug,X0,solver)

nx = numel(Xg);
nu = numel(Ug);
tau_max = 3;
Umin = Ug - tau_max;
Umax = Ug + tau_max;
feas = 1;

%Solver parameters
tf = 3;
fs = 100;
Ts = 1/fs;
kf = tf*fs;

%use lqr around goal to find an initial guess 
A = jacobianX(Xg);
B = jacobianU();
Q = diag([10 1]);
R = 20;
N = [zeros(nx,nu)];
[K,S,E] = lqr(A,B,Q,R,N);
Xguess(:,1) = X0;
Uguess = zeros(nu,kf);
[A_d,B_d] = discretizeModel(A,B,Ts);
for i = 1:kf
    Uguess(:,i) = Ug - K*(Xguess(:,i) - Xg);
    Xguess(:,i+1) = Xg + A_d*(Xguess(:,i) - Xg) + B_d*Uguess(:,i);
end


%Find better solution
cost(1) = 1e10;
k = 1;
figure(1); 
subplot(3,1,1); hold off;
subplot(3,1,2); hold off;
subplot(3,1,3); hold off;
fig = figure(2);
% set (fig, 'Units', 'normalized', 'Position', [0,0,1,1]);
plot(Xg(1),Xg(2),'mx','markersize',12,'linewidth',5); 
plotdom = [-5 10 -20 20];
% pause;
while(1)
    alpha = 1;
    U = sdpvar(nu,kf);
    X = sdpvar(nx,kf+1);
    Eqs = [];
    Ineqs = [];
    tic;
    [X_prop] = integrateODE(0,Ts,Xguess(:,1:end-1),Uguess);
    toc;

    Xbar = X - Xguess;
    Ubar = U - Uguess;

    for j = 1:kf
        Abar = jacobianX(Xguess(:,j));
        Bbar = jacobianU();
        [A_d,B_d] = discretizeModel(Abar,Bbar,Ts);
        Eqs = [Eqs;
               X(:,j+1) - X_prop(:,j) - (A_d*Xbar(:,j) + B_d*Ubar(:,j)) == 0];
        Ineqs = [Ineqs;
                 U(:,j) <= tau_max;
                 U(:,j) >= -tau_max];
    end

    %Enforce initial and final conditions 
    Eqs = [Eqs;
           X(:,1) == X0;
           X(:,kf+1) == Xg;
           U(:,kf) == Ug];

    %Minimizing function and constraints
    Xg_rep = repmat(Xg,1,kf+1);
    X_e = X - Xg_rep;
    func = U*R*U';
    Constraints = Eqs;

    tic
    ops = sdpsettings('solver',solver);
    sol = optimize(Constraints,func,ops);

    if (sol.problem == 0)
        display('Solution found');
        k = k + 1
        cost(k) = double(func)*Ts;
    else
        display('Solution not found');
        feas = 0;
        break
    end
    toc
    Xnew = alpha*double(X) + (1-alpha)*Xguess;
    Unew = alpha*double(U) + (1-alpha)*Uguess;
    while((cost(k) >= cost(k-1)) && (alpha > 0))
        alpha = alpha/2;
        Xnew = alpha*double(X) + (1-alpha)*Xguess;
        Unew = alpha*double(U) + (1-alpha)*Uguess;
        X_e = Xnew - Xg_rep;
        cost(k) = (Unew*R*Unew')*Ts;
    end

    Xguess = Xnew;
    Uguess = Unew;
    
    time = 0:Ts:tf;
    u = [time(1:end-1)' double(Uguess)'];
    figure(1); 
    subplot(3,1,1);plot(time,Xguess(1,:)); hold all; 
    subplot(3,1,2);plot(time,Xguess(2,:)); hold all; 
    subplot(3,1,3);plot(time(1:end-1),Uguess); hold all; 
    figure(2); 
    plot(Xguess(1,:),Xguess(2,:)); hold all; axis(plotdom);
    
    delta = norm(cost(k) - cost(k-1))
    if(delta < 1e-2)
        break;
    end

    pause(0.01);
end


%Constrain maximum input
while(1)
    alpha = 1;
    U = sdpvar(nu,kf);
    X = sdpvar(nx,kf+1);
    Eqs = [];
    Ineqs = [];
    tic;
    [X_prop] = integrateODE(0,Ts,Xguess(:,1:end-1),Uguess);
    toc;

    Xbar = X - Xguess;
    Ubar = U - Uguess;

    for j = 1:kf
        Abar = jacobianX(Xguess(:,j));
        Bbar = jacobianU();
        [A_d,B_d] = discretizeModel(Abar,Bbar,Ts);
        Eqs = [Eqs;
               X(:,j+1) - X_prop(:,j) - (A_d*Xbar(:,j) + B_d*Ubar(:,j)) == 0];
        Ineqs = [Ineqs;
                 U(:,j) <= tau_max;
                 U(:,j) >= -tau_max];
    end

    %Enforce initial and final conditions 
    Eqs = [Eqs;
           X(:,1) == X0;
           X(:,kf+1) == Xg;
           U(:,kf) == Ug];

    %Minimizing function and constraints
    Xg_rep = repmat(Xg,1,kf+1);
    X_e = X - Xg_rep;
    func = U*R*U';
    Constraints = [Eqs; Ineqs];

    tic
    ops = sdpsettings('solver',solver);
    sol = optimize(Constraints,func,ops);

    if (sol.problem == 0)
        display('Solution found');
        k = k + 1
        cost(k) = double(func)*Ts;
    else
        display('Solution not found');
        feas = 0;
        break
    end
    toc
    Xnew = alpha*double(X) + (1-alpha)*Xguess;
    Unew = alpha*double(U) + (1-alpha)*Uguess;
    while((cost(k) >= cost(k-1)) && (alpha > 0))
        alpha = alpha/2;
        Xnew = alpha*double(X) + (1-alpha)*Xguess;
        Unew = alpha*double(U) + (1-alpha)*Uguess;
        X_e = Xnew - Xg_rep;
        cost(k) = (Unew*R*Unew')*Ts;
    end

    Xguess = Xnew;
    Uguess = Unew;
    
    time = 0:Ts:tf;
    u = [time(1:end-1)' double(Uguess)'];
    subplot(3,1,1);plot(time,Xguess(1,:)); hold all; 
    subplot(3,1,2);plot(time,Xguess(2,:)); hold all; 
    subplot(3,1,3);plot(time(1:end-1),Uguess); hold all; 
    
    delta = norm(cost(k) - cost(k-1))
    if(delta < 1e-2)
        break;
    end

    pause(0.01);
end

% close all;

