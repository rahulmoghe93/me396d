clc;
close all;
clear all;
opts = sosoptions;

%Calculate LQR around goal
Xg = [pi; 0];   %Goal: theta = pi, thetaDot = 0;
Ug = 0;         %Equilibrium input at the goal
nx = numel(Xg);
nu = numel(Ug);
X0 = Xg + [0; 0];
tau_max = 3;
Umin = Ug - tau_max;
Umax = Ug + tau_max;

A = jacobianX(Xg);
B = jacobianU();
Q = diag([10 1]);
R = 20;
N = [zeros(nx,nu)];

[K,S,E] = lqr(A,B,Q,R,N);

%Find ROA
syms theta thetaDot tau x1 x2 real
load('NL_Model.mat');
X = [x1; x2];
u = Ug - K*(X - Xg);
Nf = 3; %Taylor expansion order
Fcl = subs(F,[theta thetaDot tau],[x1 x2 u]); %F closed loop
Fcl_minus = subs(F,[theta thetaDot tau],[x1 x2 Umin]); %F closed loop for minimum input
Fcl_plus = subs(F,[theta thetaDot tau],[x1 x2 Umax]); %F closed loop for maximum input

%% Use SOS (need to convert syms to pvar first)
clear x1 x2
pvar x1bar x2bar
epsilon = 1e-6;

Xbar = [x1bar; x2bar];
Ubar = Ug - K*Xbar;
Fcl_hat = taylorExp(Fcl,Xg); %Approximated dynamics 3rd order
Fcl_minus_hat = taylorExp(Fcl_minus,Xg);
Fcl_plus_hat = taylorExp(Fcl_plus,Xg);
Jstar = Xbar'*S*Xbar;
Jdot_hat = 2*Xbar'*S*Fcl_hat
Jdot_minus_hat = 2*Xbar'*S*Fcl_hat;
Jdot_plus_hat = 2*Xbar'*S*Fcl_hat;
L1 = epsilon*(Xbar'*Xbar);

%% Find minimum level sets for input constraints
rhoMin = levelSets(S,K,Umin,Ug);
rhoMax = levelSets(S,K,Umin,Ug);

%Do line search on rho
rho = 0;
bestRho = rho;
deltarho = 10;
while(1)
%     rho
    G = rho - Jstar;
    z = monomials(Xbar,0:6);
    for i = 1:7
        h(i) = polydecvar('c',z,'vec');
        const1(i) = (h(i) >= 0);
    end
    
    const2 = (Jdot_hat + h(3)*G + h(4)*(Ubar - Umin) ...
                 + h(5)*(Umax - Ubar) + L1 <= 0);
             
    if (rho > rhoMin)
        const2 = [const2;
                  (Jdot_minus_hat + h(1)*G + h(2)*(Umin-Ubar) + L1 <= 0)];
    end
    if (rho > rhoMax)
        const2 = [const2;
                  (Jdot_plus_hat + h(6)*G + h(6)*(Ubar - Umax) + L1 <= 0)];
    end

    const = [const1; const2];
    [info,dopt,sossol] = sosopt(const,z,[],opts);
    if (info.feas == 1)
        bestRho = rho
        rho = rho + deltarho;
    else
        if (deltarho > 1e-1)
            deltarho = deltarho/2;
            rho = bestRho + deltarho;
        else
            break;
        end
    end
    
end
save('goalLQR.mat','K','S','E','bestRho','Xg');

%% Plot ROA contour
plotdom = [-5 10 -20 20];
Ellipse = (Xbar - Xg)'*S*(Xbar - Xg);
pcontour(Ellipse,bestRho,plotdom,'k',200); hold on; grid on;
axis(plotdom);

%% Load samples and plot
load('ROA_Samples.mat');
plot(Xinit(1,:),Xinit(2,:), 'r*',ROA(1,:),ROA(2,:),'b*'); hold all;
plot(Xg(1),Xg(2),'mx','markersize',12,'linewidth',5); 
ylabel('$$\dot{\theta}$$','Interpreter','Latex'); 
xlabel('\theta');
grid on;