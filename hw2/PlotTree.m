clc;
close all;
clear all;

%Plot LQR tree so far
load('goalLQR.mat');
load('GuessSol.mat');
load('tubeSet.mat');
pvar x1bar x2bar
Xbar = [x1bar; x2bar];
%% Plot ROA contour
plotdom = [-5 10 -20 20];
Ellipse = (Xbar - Xg)'*S*(Xbar - Xg);
pcontour(Ellipse,8.75,plotdom,'k',100); hold all; grid on;
plot(Xg(1),Xg(2),'mx','markersize',12,'linewidth',5); 
axis(plotdom);

% %% Load samples and plot
% load('ROA_Samples.mat');
% plot(Xinit(1,:),Xinit(2,:), 'r*',ROA(1,:),ROA(2,:),'b*'); hold all;
% plot(Xg(1),Xg(2),'mx','markersize',12,'linewidth',5); 
% ylabel('$$\dot{\theta}$$','Interpreter','Latex'); 
% xlabel('\theta');
% grid on;

%% Plot tube
fig = figure(1);
set (fig, 'Units', 'normalized', 'Position', [0,0,1,1]);
colors = 'bgrcm';
% pause;
for i = 1:numel(tubes)
    color = colors(mod(i,numel(colors)-1)+1);
    plot(Xsol(1,:,i),Xsol(2,:,i),color,'Linewidth',2);
    tube = tubes(i).tube;
    rhomax = 100;
    for k = 1:numel(tube)
        plotdom = [-5 10 -20 20];
        Ellipse = (Xbar - tube(k).pos)'*tube(k).S*(Xbar - tube(k).pos);
        rhomax = min(rhomax, tube(k).bestRho);
        pcontour(Ellipse,rhomax,plotdom,color,150); hold all; grid on;
        pause(0.001);
    end
end