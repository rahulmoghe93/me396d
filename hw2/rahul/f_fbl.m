function xdot = f_fbl(t,x)

	global 		b
	global 		l
	global 		m
	global 		g
	global 		umax
	global 		umin
	global 		xG
	global 		uguess

	th = x(1);
	th_dot = x(2);

	% u = (b*th_dot + m*g*l*sin(th))*(m*l*l) - k1*(th-xG(1)) - k2*(th_dot-xG(2));

	xdot = 	[th_dot;...
			(-b*th_dot - m*g*l*sin(th))/(m*l*l) + uguess/(m*l*l)];
end