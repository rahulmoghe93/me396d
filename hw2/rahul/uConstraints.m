function [rhomax,rhomin] = uConstraints(S,K,umax,umin,uG)
	cvx_begin quiet
		variable x(2)
		minimize(x'*S*x)
		subject to
		umax + K*x == uG
	cvx_end
	rhomax = cvx_optval;
	cvx_begin quiet
		variable x(2)
		minimize(x'*S*x)
		subject to
		umin + K*x == uG
	cvx_end
	rhomin = cvx_optval;
end