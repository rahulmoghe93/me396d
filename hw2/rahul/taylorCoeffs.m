function [c1,c2,c3] = taylorCoeffs(f,q)
	f2_cl4 = taylor(f,q,'ExpansionPoint',[0;0],'Order', 4); % Taylor series expansion
	f2_cl3 = taylor(f,q,'ExpansionPoint',[0;0],'Order', 3); % Taylor series expansion
	f2_cl2 = taylor(f,q,'ExpansionPoint',[0;0],'Order', 2); % Taylor series expansion
	f2_cl1 = taylor(f,q,'ExpansionPoint',[0;0],'Order', 1); % Taylor series expansion
	c3 = double(coeffs(f2_cl4 - f2_cl3));
	c2 = double(coeffs(f2_cl3 - f2_cl2));
	c1 = double(coeffs(f2_cl2 - f2_cl1));
	if isempty(c1)
		c1 = 0;
	end
	if isempty(c2)
		c2 = 0;
	end
	if isempty(c3)
		c3 = 0;
	end
end