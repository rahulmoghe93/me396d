function X = returnPolyFromTime(traj,t)

n_piece = numel(traj.t0);

%Find which piece of polynomial should be used
if (t < traj.t0(1))
    piece = 1;
elseif (t > traj.tf(n_piece))
    piece = n_piece;
else
	% piece = max(find(traj.t0 <= t));
    for i = n_piece:-1:1
        if (t >= traj.t0(i))
            piece = i;
            break;
        end
    end
end

poly_order = size(traj.polyCoeff,1);
for j = 1:poly_order
    timeMatrix(1,poly_order+1-j) = t^(j-1);
end

X = timeMatrix*traj.polyCoeff(:,piece);