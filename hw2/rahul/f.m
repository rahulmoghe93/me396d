function xdot = f(t,x)
	global b l m g K umax umin
	theta = x(1);
	theta_dot = x(2);
	u = - K(1)*(theta-pi) - K(2)*theta_dot;
	if u >umax
		u = umax;
	elseif u< umin
		u = umin;
	end
	xdot = 	[theta_dot;
			(-b*theta_dot - m*g*l*sin(theta) + u)/(m*l*l)];
end