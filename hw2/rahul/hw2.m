clear all; close all; clc
% -------------------- Main File for HW 2 ---------------------
fprintf('Running Time Invariant SOS Verification...\n')
pendulum
fprintf('Press any key to continue...\n')
pause

fprintf('Running SQP Solver with line-search on final time...\n')

SQP
fprintf('Press any key to continue...\n')
pause

fprintf('Solving Time Varying LQR and SOS Prep function...\n')
%% ---------------------- Buttons for Plotting ----------------------
% 
% 							0 ==> No Plotting
% 
plotTrajFit = 0;
plotSFit = 0;

sosPrep
fprintf('Press any key to continue...\n')
pause

fprintf('Solving Time Varying LQR Verification ...\n')

sosoptlqr

fprintf('Press any key to continue...\n')
pause

fprintf('Solving SOS ...\n')
timeLQRVerification