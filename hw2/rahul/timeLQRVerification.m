%% --------------------- Time Varying LQR SOS Verification -------------------------
clear t
t = T(k);

opts = sosoptions;
rho = 0;
drho = 1;
bestrho = rho;
clear pconstr h
fprintf('\n\t\t...Starting SOS with line-search on rho...\n')
% while drho > 1e-3
	% rho
	z = monomials([xbar1;xbar2],0:5);
	for i =1:2
		h(i) = polydecvar(strcat('d',num2str(i)),z,'vec');
		pconstr(i) = h(i) >= 0;
	end
	h(3) = polydecvar(strcat('d',num2str(i)),z,'vec');
	pconstr(3) = Jdot <= 0;%+ h(3)*(rho - J) + h(2)*(t - T(k)) + h(1)*(T(k+1) - t) <= 0;
	% if (rho > rhomax) && (rho > rhomin)
	% 	pconstr(8) = Jdotmin + h(1)*(rho - J) + h(2)*(umin - uG + K(k,:)*x) <= 0
	% 	pconstr(9) = Jdotmax + h(6)*(rho - J) + h(7)*(umax - uG + K(k,:)*x) <= 0
	% end
	[info,dopt,sossol] = sosopt(pconstr,z,[], opts);
	if info.feas
		bestrho = rho;
		rho = rho + drho;
	else
		drho = drho/2;
		rho = bestrho;
		rho = rho + drho;
	end
% end
% bestrho