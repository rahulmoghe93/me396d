clear polytime polyt at a
%% ---------------------- Fitting coeffs as functions of time ----------------------
k = 10;
n_t = 5;
% syms t real
t = T(k);
for i = 1:n_t+1
 	polytime(1,n_t+2-i) = t^(i-1);
end

for i = 1:size(c,1)
	for j = 1:size(c,2)
		a = reshape(c(i,j,:),N,1);
		at(i,j) = fitPiecewisePoly(n_t,N,T,a);
		polyt(i,j) = polytime*at(i,j).polyCoeff(:,k);
	end
end
Jdot = sum(sum(polyt.*tt));

% for i = 1:size(cmin,1)
% 	for j = 1:size(cmin,2)
% 		a = reshape(cmin(i,j,:),N,1);
% 		at(i,j) = fitPiecewisePoly(n_t,N,T,a);
% 		polyt(i,j) = polytime*at(i,j).polyCoeff(:,k);
% 	end
% end
% Jdotmin = sum(sum(polyt.*ttmin));

% for i = 1:size(cmax,1)
% 	for j = 1:size(cmax,2)
% 		a = reshape(cmax(i,j,:),N,1);
% 		at(i,j) = fitPiecewisePoly(n_t,N,T,a);
% 		polyt(i,j) = polytime*at(i,j).polyCoeff(:,k);
% 	end
% end
% Jdotmax = sum(sum(polyt.*ttmax));

%% ---------------------- SOSOPT Finally ----------------------

pvar xbar1 xbar2 %t
x = [xbar1;xbar2];
Jdot = eval(Jdot);
% Jdotmax = eval(Jdotmax);
% Jdotmin = eval(Jdotmin);
% zt = flipud(monomials(t,0:5));
zt = [T(k)^5;T(k)^4;T(k)^3;T(k)^2;T(k);1];
Sfit = [zt'*straj(1).polyCoeff(:,k) zt'*straj(2).polyCoeff(:,k);
		zt'*straj(2).polyCoeff(:,k) zt'*straj(3).polyCoeff(:,k)];
J = x'*Sfit*x;

%% --------------------- Finding the input constraint level sets ------------------
% Sfitpoly = [spoly(k,1) spoly(k,2); spoly(k,2) spoly(k,3)];
% [rhomax, rhomin] = uConstraints(Sfitpoly,K(k,:),umax,umin,uG);