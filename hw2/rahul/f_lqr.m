function Sdot = f_lqr(t,Si)
	global 		Q
	global 		R
	global 		xtraj
	global 		g
	global 		l
	global 		m
	global 		b
	
	x1t = returnPolyFromTime(xtraj(1),t);
	Si = reshape(Si,2,2);

	A = [0 1; -g*cos(x1t)/l -b/(m*l*l)];
	B = [0; 1/(m*l*l)];
	Sdot = -Q+Si*B*inv(R)*B'*Si - Si*A - A'*Si;
	Sdot = Sdot(:);
end