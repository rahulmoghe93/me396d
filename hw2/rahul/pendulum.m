%% -------------- Loading Data ------------------
global m l g b K umax umin xG Q R
m = 1;
l = 0.5;
b = 0.1;
g = 9.8;
xG = [pi;0];
uG = 0;
Q = diag([10 1]);
R = 20;
nx = 2;
nu = 1;
Nf = 2;
Nt = 3;
umax = 3;
umin = -3;
N = zeros(nx,nu); % The 3rd term(cross term) in the LQR Cost Function

%% --------------------- Linearizing the System -------------------------
A = [0 1; g/l -b/(m*l*l)];
B = [0 ; 1/(m*l*l)];

%% --------------------- LQR -------------------------
[K,S,E] = lqr(A,B,Q,R,N);

%% --------------------- Polynomial Approxiamtion for closed loop f -------------------------
q = sym('q',[2 1]);
f1_cl = symfun(	q(2), q); 													% Define dynamics symbolically 
f2_cl = symfun(	(-b*q(2) + m*g*l*sin(q(1)) - K*q)/(m*l*l), q); 				% Define dynamics symbolically 
f2_cl_umax = symfun((-b*q(2) + m*g*l*sin(q(1)) + umax )/(m*l*l), q); 		% Define dynamics symbolically 
f2_cl_umin = symfun((-b*q(2) + m*g*l*sin(q(1)) + umin)/(m*l*l), q); 		% Define dynamics symbolically 
[a1,a2,a3] = taylorCoeffs(f1_cl,q);
[b1,b2,b3] = taylorCoeffs(f2_cl,q);
[c1,c2,c3] = taylorCoeffs(f2_cl_umax,q);
[d1,d2,d3] = taylorCoeffs(f2_cl_umin,q);

%% --------------------- LQR SOS Verification -------------------------
epsilon = 1e-6;
pvar x1 x2
x = [x1;x2];
opts = sosoptions;
rho = 0;
drho = 1;
bestrho = rho;

%% --------------------- Finding the input constraint level sets -------------------------
[rhomax, rhomin] = uConstraints(S,K,umax,umin,uG);

%% --------------------- SOS Linesearch for ROA -------------------------
while drho > 1e-2
	rho
	clear x1 x2
	pvar x1 x2
	x = [x1;x2];
	z = monomials([x1;x2],0:4);
	for i =1:7
		h(i) = polydecvar(strcat('d',num2str(i)),z,'vec');
		pconstr(i) = h(i) >= 0;
	end
	fcl 	= 	[x2;		b1(1)*x2 + b1(2)*x1 + b3*x1^3];
	fclmax 	= 	[x2;		c1(1)*x2 + c1(2)*x1 + c3*x1^3];
	fclmin 	= 	[x2;		d1(1)*x2 + d1(2)*x1 + d3*x1^3];
	Jdot = 2*x'*S*fcl;
	Jdotmin = 2*x'*S*fclmin;
	Jdotmax = 2*x'*S*fclmax;
	J = x'*S*x;
	pconstr(8) = Jdot + h(3)*(rho - J) + h(4)*(uG - K*x - umin) + h(5)*(umax - uG + K*x) <= -epsilon*x'*x ;
	if (rho > rhomax) && (rho > rhomin)
		pconstr(9) = Jdotmin + h(1)*(rho - J) + h(2)*(umin - uG + K*x) <= -epsilon*x'*x ;
		pconstr(10) = Jdotmax + h(6)*(rho - J) + h(7)*(umax - uG + K*x) <= -epsilon*x'*x ;
	end
	[info,dopt,sossol] = sosopt(pconstr,z,[], opts);
	if info.feas
		bestrho = rho;
		rho = rho + drho;
	else
		drho = drho/2;
		rho = bestrho;
		rho = rho + drho;
	end
end

%% --------------------- Plotting random samples to compare -------------------------
k = 1;
N = 1000;
x_reach = [];
x_notreach = [];
while k < N
	x0 = 8*randn(2,1);
	[T,X] = ode45(@f,[0 20], x0);
	if (norm(X(end,:) - xG',2) < 1e-6)
		x_reach = [x_reach x0];
	else
		x_notreach = [x_notreach x0];
	end
	k = k + 1;
end

% Plot ROA contour
figure(1)
[V,D] = eig(S);
t = linspace(0,2*pi,100);
a = sqrt(rho/D(1,1));
d = sqrt(rho/D(2,2));
y = V'*[a*sin(t);d*cos(t)] + xG;
patch(y(1,:),y(2,:),[0.5 0.5 0.5])
hold on; grid on;
axis([-1 7 -15 15])

% plotdom = [-5 10 -25 25];
% Ellipse = (x - xG)'*S*(x - xG);
% pcontour(Ellipse,bestrho,plotdom,'k',200); hold on; grid on;
% axis(plotdom);

if ~isempty(x_reach)
	plot(x_reach(1,:),x_reach(2,:), 'b.','Markersize',10);
end
if ~isempty(x_notreach)
	plot(x_notreach(1,:),x_notreach(2,:),'r*','Markersize',5,'Linewidth',2);
end

clear q1 q2 x1 x2 Jdot J f_cl