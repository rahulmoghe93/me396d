%% ---------------------- Buttons for Plotting ----------------------
% 
% 							0 ==> No Plotting
% 
plotTrajFit = 0;
plotSFit = 0;
clear c cmin cmax Jd Jdmin Jdmax tt ttmin ttmax
%% ---------------------- Fitting Polynomial for trajectory ----------------------
time = 0:dt:tfopt;
time = time';
tu = time(1:end-1);
n_piece = 5;
n_degree = 5;

xtraj(1) 	= fitPiecewisePoly(n_degree,n_piece,time,xiopt(:,1));
xtraj(2) 	= fitPiecewisePoly(n_degree,n_piece,time,xiopt(:,2));
utraj 		= fitPiecewisePoly(n_degree,n_piece,tu,muopt);

%% ---------------------- Time Varying LQR ----------------------
global 		xtraj
Qf = S;
[T,St] = ode45(@f_lqr,[tfopt 0],Qf(:));
T  = flipud(T);
St = flipud(St);
%% ---------------------- Piecewise Polynoimial fit for S(t) ----------------------
N = length(T);			% Number of pieces for S(t) and \rho (t)
n_order = 5;	% Order of the polynomial for each piece

straj(1) = fitPiecewisePoly(n_order,N,T,St(:,1));
straj(2) = fitPiecewisePoly(n_order,N,T,St(:,2));
straj(3) = fitPiecewisePoly(n_order,N,T,St(:,4));

for i = 1:length(T)
	% 	Finding K(t) from the Riccati Equation solution
	K(i,:) = inv(R)*B'*reshape(St(i,:),2,2);
	Sdot(i,:) = f_lqr(T(i),St(i,:));
	%	Finding the trajectory at discrete points
	spoly(i,1) = returnPolyFromTime(straj(1),T(i));
	spoly(i,2) = returnPolyFromTime(straj(2),T(i));
	spoly(i,3) = returnPolyFromTime(straj(3),T(i));
	xpoly(i,1) = returnPolyFromTime(xtraj(1),T(i));
	xpoly(i,2) = returnPolyFromTime(xtraj(2),T(i));
	upoly(i)  = returnPolyFromTime(utraj,T(i));
end

if plotSFit
	figure(4)
	clf;
	subplot(4,1,1)
	plot(T,spoly,'r','Linewidth',2)
	hold on
	plot(T,St(:,1),'b--','Linewidth',2)

	subplot(4,1,2)
	plot(T,spoly,'r','Linewidth',2)
	hold on
	plot(T,St(:,2),'b--','Linewidth',2)

	subplot(4,1,3)
	plot(T,spoly,'r','Linewidth',2)
	hold on
	plot(T,St(:,3),'b--','Linewidth',2)

	subplot(4,1,4)
	plot(T,spoly,'r','Linewidth',2)
	hold on
	plot(T,St(:,4),'b--','Linewidth',2)
end

if plotTrajFit
	figure(3)
	clf;
	subplot(3,1,1)
	plot(T,xpoly,'r','Linewidth',2)
	hold on
	plot(T,xiopt(:,1),'b--','Linewidth',2)
	subplot(3,1,2)
	plot(T,xpoly,'r','Linewidth',2)
	hold on
	plot(T,xiopt(:,2),'b--','Linewidth',2)
	subplot(3,1,3)
	plot(T,upoly,'r','Linewidth',2)
	hold on
	plot(tu,muopt,'b--','Linewidth',2)
end

%% ---------------------- Finding Jd as a polynomial Function ----------------------
% 		Find Jd as a symfun of xbar at time in T
xbar = sym('xbar',[2 1]);
assume(xbar,'real');
bb = m*l*l;
for i = 1:length(T)
	% fsos = symfun([xbar(2) + xpoly(i,2); (-b*xbar(2) - b*xpoly(i,2) - ...
	% 			m*g*l*sin(xbar(1)+xpoly(i,1))...
	% 			+ upoly(i) - K(i,1)*xbar(1) - K(i,2)*xbar(2))/(m*l*l)],xbar);
	fsos = symfun([xbar(2); -b*xbar(2)/bb - ...
				m*g*l*sin(xbar(1)+xpoly(i,1))/bb - m*g*l*sin(xpoly(i,1))/bb...
				- K(i,1)*xbar(1)/bb - K(i,2)*xbar(2)/bb],xbar);
	% fsosmin = symfun([xbar(2) + xpoly(i,2); (-b*xbar(2) - b*xpoly(i,2) - ...
	% 			m*g*l*sin(xbar(1)+xpoly(i,1))...
	% 			+ umin)/(m*l*l)],xbar);
	% fsosmax = symfun([xbar(2) + xpoly(i,2); (-b*xbar(2) - b*xpoly(i,2) - ...
	% 			m*g*l*sin(xbar(1)+xpoly(i,1))...
	% 			+ umax)/(m*l*l)],xbar);
	Jd = xbar'*reshape(Sdot(i,:),2,2)*xbar + 2*xbar'*reshape(St(i,:),2,2)*fsos;
	% Jdmin = xbar'*reshape(Sdot(i,:),2,2)*xbar + 2*xbar'*reshape(St(i,:),2,2)*fsosmin;
	% Jdmax = xbar'*reshape(Sdot(i,:),2,2)*xbar + 2*xbar'*reshape(St(i,:),2,2)*fsosmax;
	Jd_taylor = taylor(Jd,xbar,'ExpansionPoint',[0;0],'Order', 5);
				% -taylor(Jd,xbar,'ExpansionPoint',[0;0],'Order', 2);			% Taylor series expansion
	% Jdmin_taylor = taylor(Jdmin,xbar,'ExpansionPoint',[0;0],'Order', 3);	% Taylor series expansion
	% Jdmax_taylor = taylor(Jdmax,xbar,'ExpansionPoint',[0;0],'Order', 3);	% Taylor series expansion
	[c(:,:,i),tt] = coeffs(Jd_taylor,xbar,'All');
	% [cmin(:,:,i),ttmin] = coeffs(Jdmin_taylor,xbar,'All');
	% [cmax(:,:,i),ttmax] = coeffs(Jdmax_taylor,xbar,'All');
end
fprintf('\nPress key to continue...\n')
pause
sosoptlqr