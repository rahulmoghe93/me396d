function xnext = propagateXi(dt,Xi)
	[T, X] = ode45(@f_fbl,[0 dt],Xi);
	xnext = X(end,:)';