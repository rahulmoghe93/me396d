function [xi,mu] = initGuess(x0,n,dt)
	global 	b
	global 	l
	global 	m
	global 	g
	global 	K0
	global 	xG

	k1 = K0(2,1);
	k2 = K0(2,2);

	xbar = x0 - xG;

	mu(1,1) = b*x0(2) + m*g*l*sin(x0(1)) - m*l*l*(k1*xbar(1) + k2*xbar(2));
	xi(1,:) = x0(:)';
	for i = 1:n
		xi(i+1,:) = expm(K0*i*dt)*(x0-xG) + xG;
		mu(i+1,1) = b*xi(i,2) + m*g*l*sin(xi(i,1)) - m*l*l*(k1*(xi(i,1)-xG(1)) + k2*(xi(i,2)-xG(2)));
	end