% close all;
%% ---------------------- SQP Solver ----------------------
global 		K0
global 		uguess

tf = 4;
dt = 0.05;
iter = 8;
K0 = [0 1; -5 -10];
x0 = [5; 8];
opt = 100000;
tfopt = 100;
%% ---------------------- Line-search on t final with SQP ----------------------
while tf > 1
	n = tf/dt;
	[xi0,mu0] = initGuess(x0,n,dt);
	xi = xi0;
	mu = mu0;
	for j = 1:iter
		cvx_begin quiet
			variable x(n+1,2)
			variable u(n)
			minimize(u'*u)
			for i = 1:n
				uguess = mu(i);
				xi_plus = propagateXi(dt,xi(i,:));
				Ac = [0 1; -g*cos(xi(i,1))/l -b/(m*l*l)];
				Bc = [0; 1/(m*l*l)];
				[A,B] = discretizeModel(Ac,Bc,dt);
				subject to
					x(i+1,:)' - xi_plus == A*(x(i,:)' - xi(i,:)') + B*(u(i) - mu(i));
					u(i) <= umax;
					u(i) >= umin;
			end
			subject to
				x(1,:) == x0';
				x(end,:) == xG';
				u(n) == uG;
		cvx_end
		if cvx_optval == Inf
			tf = 0;
			break;
		end
		fprintf('Opt value + t final = %4f\t',cvx_optval + tf);
		fprintf('for t final = %4f\n',tf);
		xi = x;
		mu = u;
		if cvx_optval + tf < opt
			opt = cvx_optval + tf;
			tfopt = tf;
			xiopt = x;
			muopt = u;
		end
	end
	tf = tf - 1;
end

figure(2)
subplot(2,1,1)
plot(xiopt(:,1),'k','Linewidth',2)
hold on
plot(xiopt(:,2),'g','Linewidth',2)
title('Optimal States','Fontsize',14.5)
subplot(2,1,2)
plot(muopt,'r','Linewidth',2)
title('Control Input for the Optimal Solution','Fontsize',14.5)

figure(1)
% clf;
% plot(xi0(:,1),xi0(:,2),'r--','Linewidth',2)
hold on
plot(xiopt(:,1),xiopt(:,2),'b','Linewidth',2)
plot(x0(1),x0(2),'kx','Markersize',10,'Linewidth',4)
plot(xG(1),xG(2),'gx','Markersize',10,'Linewidth',4)
% title('Phase Space Trajectory for Final Iteration','Fontsize',14.5)
% legend('Initial Guess','Final Solution','Initial Point','Goal State')