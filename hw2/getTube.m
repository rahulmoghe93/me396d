function [tube trajK] = getTube(Q,R,Qf,traj)

[trajS trajK] = LTV_LQR_poly(Q,R,Qf,traj);


n = size(trajS(1,1).polyCoeff,1);       %Polynomial order
n_piece = size(trajS(1,1).polyCoeff,2); %Number of polynomial pieces

%Create vector of time polynomials
syms t real
for i = 1:n
    Cp(1,n+1-i) = t^(i-1);
end
Cd = diff(Cp,t);

%Get all pieces of S and Sdot in terms of t
clear S;
for i = 1:n_piece
    S(i).mat = [Cp*trajS(1,1).polyCoeff(:,i) Cp*trajS(1,2).polyCoeff(:,i);
                Cp*trajS(2,1).polyCoeff(:,i) Cp*trajS(2,2).polyCoeff(:,i)];
    S(i).t0 = trajS(1,1).t0(i);
    S(i).tf = trajS(1,1).tf(i);
    S(i).n = n;
    Sdot(i).mat = [Cd*trajS(1,1).polyCoeff(:,i) Cd*trajS(1,2).polyCoeff(:,i);
                   Cd*trajS(2,1).polyCoeff(:,i) Cd*trajS(2,2).polyCoeff(:,i)];
    Xt(:,i) = [Cp*traj(1).polyCoeff(:,i); Cp*traj(2).polyCoeff(:,i)];
    Ut(1,i) = Cp*traj(3).polyCoeff(:,i);
end

k = 1;
for i = n_piece:-1:1
    syms theta thetaDot tau x1bar x2bar real
    load('NL_Model.mat');
    Xbar = [x1bar; x2bar];
    piece = i
    B = jacobianU;
    K = inv(R)*B'*S(piece).mat;
    u = Ut(1,piece) - K*Xbar;
    Fcl = subs(F,[theta; thetaDot; tau],[Xt(:,piece)+Xbar; u]);
    tic
%     [Jdot_syms] = getJdot(Fcl,S(piece),Sdot(piece),S(piece).t0);
    toc
    
    pvar x1bar x2bar beta
    Xbar = [x1bar; x2bar];
    
    dt = (S(piece).tf-S(piece).t0)/10;
    t_vec = S(piece).tf-dt:-dt:S(piece).t0

    for j = 1:numel(t_vec)
        t = t_vec(j)
        [Jdot_syms] = getJdot(Fcl,S(piece),Sdot(piece),t);
        pvar x1bar x2bar beta
        Xbar = [x1bar; x2bar];
        bestRho = sos_tube(S,t,piece,Jdot_syms)

        tube(k).pos = eval(Xt(:,piece));
        tube(k).t = t;
        tube(k).S = eval(S(piece).mat);
        tube(k).bestRho = bestRho;
        k = k+1;
    end
    
end