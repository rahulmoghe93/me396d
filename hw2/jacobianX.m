function A = jacobianX(X)

theta = X(1);

A = [                  0,    1;
      -(98*cos(theta))/5, -2/5];