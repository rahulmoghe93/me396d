function f = eval_f(X)

theta = X(1);
thetaDot = X(2);

f = [thetaDot;
     -(2*thetaDot)/5-(98*sin(theta))/5];