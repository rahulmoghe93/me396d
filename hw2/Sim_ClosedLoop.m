clc;
close all;
clear all;

load('polynomialTraj.mat');
load('tubeSet.mat');

traj_num = 1;

Ts = 0.01; %Sampling time for simulation
[T,Y] = solveClosedLoopSys(traj(traj_num,:),trajsK(traj_num).trajK,Ts);

figure();
subplot(2,1,1);plot(T,Y(:,1));
subplot(2,1,2);plot(T,Y(:,2));

%Animate solution
figure();
theta = Y(:,1);
l = 0.5;
n = numel(T);
axislim = 1.2*l*[-1 1 -1 1];

for i=1:n
    tic;
    x = l*sin(theta(i));
    y = -l*cos(theta(i));
    origin = [0 0];
    Xvec = [origin(1) x];
    Yvec = [origin(2) y];
    plot(Xvec,Yvec); hold all;
    plot(x,y,'o','Markersize',12,'Linewidth',5);
    axis(axislim); grid on;
    title(strcat('t = ',num2str(T(i),'%1.1f')));
    if (i == 1)
%         pause;
    else
        pause(4*Ts-toc);
    end
    hold off;
end