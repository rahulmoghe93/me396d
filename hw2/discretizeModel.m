function [Ad,Bd] = discretizeModel(A,B,Ts)

nx = size(A,1);
nu = size(B,2);

C = eye(nx);
D = zeros(nx,nu);
SYS = ss(A,B,C,D);
SYSD = c2d(SYS, Ts,'tustin');
Ad = SYSD.A;
Bd = SYSD.B;