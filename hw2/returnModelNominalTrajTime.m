function [Abar,Bbar] = returnModelNominalTrajTime(traj,time)



X1 = returnPolyFromTime(traj(1),time);
X2 = returnPolyFromTime(traj(2),time);
U = returnPolyFromTime(traj(3),time);
X = [X1; X2];
Abar = jacobianX(X);
Bbar = jacobianU();