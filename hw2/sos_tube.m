function bestRho = sos_tube(S,t,piece,Jdot_syms)

%% Use SOS to find tube
pvar x1bar x2bar beta
opts = sosoptions;
Xbar = [x1bar; x2bar];
% epsilon = 1e-6;
%     L1 = epsilon*(Xbar'*Xbar);

%Do line search on rho
rho = 0;
bestRho = rho;
deltarho = 5;

Jdot = eval(Jdot_syms);
Jstar = Xbar'*eval(S(piece).mat)*Xbar;
while(1)
%     rho
%     const1 = [];
%     const2 = [];
    G1 = rho - Jstar;
    G2 = t - S(piece).t0;
    G3 = S(piece).tf - t;
    z = monomials(Xbar,0:5);
    h1 = polydecvar('c',z,'vec');
    h2 = polydecvar('c',z,'vec');
    h3 = polydecvar('c',z,'vec');
%     const1(1) = (h1 >= 0);
    const1(1) = (h2 >= 0);
    const1(2) = (h3 >= 0);

%     const2 = (Jdot - beta + h1*G1 + h2*G2 + h3*G3 <= 0);
    const2 = (Jdot + h1*G1 <= 0);
%     const2 = (Jdot + h1*G1 + h2*G2 + h3*G3 <= 0);

    const = [const1; const2];
    [info,dopt,sossol] = sosopt(const,z,[],opts);
    if (info.feas == 1)
        bestRho = rho;
        rho = rho + deltarho;
    else
%             display('infeasible')
        if (deltarho > 1e-1)
            deltarho = deltarho/2;
            rho = bestRho + deltarho;
        else
            break;
        end
    end

end