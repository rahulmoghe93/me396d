function rho = levelSets(S,K,Umax,Ug)

nx = size(K,2);
X = sdpvar(nx,1);

func = X'*S*X;
Constraints = [Umax == Ug - K*X];

ops = sdpsettings('solver','gurobi');
sol = optimize(Constraints,func,ops);

rho = double(func);