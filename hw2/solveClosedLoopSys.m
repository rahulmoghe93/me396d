function [T,Y] = solveClosedLoopSys(traj,trajK,Ts)

% traj.t0
t0 = traj(1,1).t0(1);
tf = traj(1,1).tf(end);
X10 = returnPolyFromTime(traj(1,1),t0);
X20 = returnPolyFromTime(traj(1,2),t0);
% U0 = returnPolyFromTime(traj(1,3),t0);


X0 = [X10; X20];

options = odeset('RelTol',1e-6,'AbsTol',1e-6);
time = t0:Ts:tf;
[T,Y] = ode45(@StateEq,time,X0,options);


    function Xdot = StateEq(t,X)
        if (t <= tf)
            K1 = returnPolyFromTime(trajK(1,1),t);
            K2 = returnPolyFromTime(trajK(1,2),t);
            X1des = returnPolyFromTime(traj(1,1),t);
            X2des = returnPolyFromTime(traj(1,2),t);
            u_nom = returnPolyFromTime(traj(1,3),t);
            K = [K1 K2];
            Xdes = [X1des; X2des];
        else
            K1 = returnPolyFromTime(trajK(1,1),tf);
            K2 = returnPolyFromTime(trajK(1,2),tf);
            X1des = returnPolyFromTime(traj(1,1),tf);
            X2des = returnPolyFromTime(traj(1,2),tf);
            u_nom = returnPolyFromTime(traj(1,3),t);
            K = [K1 K2];
            Xdes = [X1des; X2des];
        end
        U = u_nom - K*(X-Xdes);
        Xdot = eval_f(X) + eval_g*U;

    end
end