function [S,time,K] = solveTimeVaryingLQR(Q,R,Qf,traj)

t0 = traj(1).t0(1);
tf = traj(1).tf(end);
Init_Val = reshape(Qf,[4 1]);

options = odeset('RelTol',1e-6,'AbsTol',1e-6);
[T,Y] = ode45(@StateEq,[tf:-0.01:t0],Init_Val,options);

n_points = numel(T);
B = jacobianU;
for i = 1:n_points
    S(:,:,n_points+1-i) = reshape(Y(i,:),[2 2]);
    time(n_points+1-i,1) = T(i);
    K(:,:,n_points+1-i) = inv(R)*B'* S(:,:,n_points+1-i);
end

    function Sdot = StateEq(t,S)
    S_mat = reshape(S,[2 2]);
    [Abar,Bbar] = returnModelNominalTrajTime(traj,t);
    
    Sdot_mat = -(Q - S_mat*Bbar*inv(R)*Bbar'*S_mat + S_mat*Abar + Abar'*S_mat);

    Sdot = reshape(Sdot_mat,[4 1]);
    end
end