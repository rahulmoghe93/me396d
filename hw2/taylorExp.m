function Fcl_hat = taylorExp(Fcl,Xg)


pvar x1bar x2bar
syms x1 x2 real

Xbar = [x1bar; x2bar];
X = [x1; x2];
nx = 2;

%Calculate partials
for i = 1:nx
    for j = 1:nx
        Df(i,j) = diff(Fcl(i),X(j));
    end
end

for i = 1:nx
    for j = 1:nx
        for k = 1:nx
            D2f(i,j,k) = diff(Df(i,j),X(k))/2;
        end
    end
end

for i = 1:nx
    for j = 1:nx
        for k = 1:nx
            for l = 1:nx
                D3f(i,j,k,l) = diff(D2f(i,j,k),X(l))/3;
            end
        end
    end
end

%Evaluate partials
Fcl = double(subs(Fcl,X,Xg));
Df = double(subs(Df,X,Xg));
D2f = double(subs(D2f,X,Xg));
D3f = double(subs(D3f,X,Xg));

Fcl_hat = polynomial(Fcl);
for i = 1:nx
    Fcl_hat(:) = Fcl_hat(:) + Df(:,i)*Xbar(i);
    for j = 1:nx
        Fcl_hat(:) = Fcl_hat(:) + D2f(:,i,j)*Xbar(i)*Xbar(j);
        for k = 1:nx
            Fcl_hat(:) = Fcl_hat(:) + D3f(:,i,j,k)*Xbar(i)*Xbar(j)*Xbar(k);
        end
    end
end
