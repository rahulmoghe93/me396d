function Jdot = getJdot(Fcl,S,Sdot,t)

syms x1bar x2bar real
xbar = [x1bar; x2bar];
Jdot_star = eval(xbar'*Sdot.mat*xbar + 2*xbar'*S.mat*Fcl);
Jdot = taylor(Jdot_star,xbar,zeros(2,1),'Order',5) - taylor(Jdot_star,xbar,zeros(2,1),'Order',2);


% syms x1bar x2bar t real
% xbar = [x1bar; x2bar];
% 
% Jdot_star = xbar'*Sdot.mat*xbar + 2*xbar'*S.mat*Fcl;
% 
% %Sample times
% dt = 0.01;
% time = S.t0:dt:S.tf;
% 
% %Sample multiple points in time and do taylor expansion
% for i = 1:numel(time)
%     t = time(i);
%     Jdot_sample = eval(Jdot_star);
%     Jdot_sample_hat = taylor(Jdot_sample,xbar,zeros(2,1),'Order',5) - taylor(Jdot_sample,xbar,zeros(2,1),'Order',2);
%     [C(:,:,i),T] = coeffs(Jdot_sample_hat,xbar,'All');
% end
% % double(C)
% % T
% 
% %Fit a time polynomial among multiple coefficients
% C = double(C);
% sizeCoeff = size(C);
% n = S.n-1;
% n_piece = 1;
% 
% syms t real
% for i = 1:n+1
%     Cp(1,n+2-i) = t^(i-1);
% end
% 
% Jdot = 0;
% for i = 1:sizeCoeff(1)
%     for j = 1:sizeCoeff(2)
%         Cij_vec = reshape(C(i,j,:),[numel(time) 1 1])';
%         trajC = fitPiecewisePoly(n,n_piece,time,Cij_vec);
%         polyC = Cp*trajC.polyCoeff;
%         Jdot = Jdot + polyC*T(i,j);
%     end
% end
