function [trajS trajK] = LTV_LQR_poly(Q,R,Qf,traj)

[S,t,K] = solveTimeVaryingLQR(Q,R,Qf,traj);

%Fit S(t) terms into polynomials
len = numel(t);
n = 10;
n_piece = 10;
S11 = reshape(S(1,1,:),[len 1 1]);
S12 = reshape(S(1,2,:),[len 1 1]);
S21 = reshape(S(2,1,:),[len 1 1]);
S22 = reshape(S(2,2,:),[len 1 1]);
trajS11 = fitPiecewisePoly(n,n_piece,t,S11);
trajS12 = fitPiecewisePoly(n,n_piece,t,S12);
trajS21 = fitPiecewisePoly(n,n_piece,t,S21);
trajS22 = fitPiecewisePoly(n,n_piece,t,S22);
trajS = [trajS11 trajS12;
         trajS21 trajS22];
     
K11 = reshape(K(1,1,:),[len 1 1]);
K12 = reshape(K(1,2,:),[len 1 1]);
trajK11 = fitPiecewisePoly(n,n_piece,t,K11);
trajK12 = fitPiecewisePoly(n,n_piece,t,K12);

trajK = [trajK11 trajK12];