clc;
close all;
clear all;

%Sequential quadratic programming
Xg = [pi; 0];   %Goal point
Ug = 0;         %Equilibrium input at the goal
X0 = [5 8;
      2 2;
      5.9 -5.27;
      -0.61 -3.37;
      -2.73 -1.014;
      7.132 1.717];
% X0 = [5 8];
n = 10;
n_piece = 10;
load('goalLQR.mat');
Q = diag([10 1])';
R = 20;
Qf = S;

k = 1;
for i = 1:size(X0,1)
    [time,Xguess(:,:,i),Uguess(:,i),feas] = solveSQP(Xg,Ug,X0(i,:)','sedumi');
    
    if(feas == 1)
        Xsol(:,:,k) = Xguess(:,:,i);
        Usol(:,k) = Uguess(:,i);
        
        %Fit solution into a polynomial
        trajX1 = fitPiecewisePoly(n,n_piece,time,Xsol(1,:,k));
        trajX2 = fitPiecewisePoly(n,n_piece,time,Xsol(2,:,k));
        trajU = fitPiecewisePoly(n,n_piece,time(1:end-1),Usol(:,k)');
        traj(k,:) = [trajX1; trajX2; trajU];
        
        %Get time varying-LQR tube
        [tubes(k).tube trajsK(k).trajK] = getTube(Q,R,Qf,traj(k,:));
        
        %Plot solution trajectory
        figure(3);
        subplot(3,1,1);plot(time,Xsol(1,:,k)); hold all; 
        subplot(3,1,2);plot(time,Xsol(2,:,k)); hold all; 
        subplot(3,1,3);plot(time(1:end-1),Usol(:,k)); hold all; 
        figure(4);
        plot(Xsol(1,:,k),Xsol(2,:,k),'Linewidth',2); hold all;
        k = k+1;
    end
end

%% Plot ROA contour
figure(4)
load('goalLQR.mat');
pvar x1bar x2bar
Xbar = [x1bar; x2bar];
plotdom = [-5 10 -20 20];
Ellipse = (Xbar - Xg)'*S*(Xbar - Xg);
pcontour(Ellipse,bestRho,plotdom,'k',200); hold all; grid on;
plot(Xg(1),Xg(2),'mx','markersize',12,'linewidth',5); 
axis(plotdom);

% %% Load samples and plot
% load('ROA_Samples.mat');
% plot(Xinit(1,:),Xinit(2,:), 'r*',ROA(1,:),ROA(2,:),'b*'); hold all;
% plot(Xg(1),Xg(2),'mx','markersize',12,'linewidth',5); 
% ylabel('$$\dot{\theta}$$','Interpreter','Latex'); 
% xlabel('\theta');
% grid on;

% Save solution
save('GuessSol.mat','Xsol','Usol','time');
save('polynomialTraj.mat','traj');
save('tubeSet.mat','tubes','trajsK');
