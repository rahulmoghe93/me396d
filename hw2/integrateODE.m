function [X_prop] = integrateODE(t0,tf,X,u)

nx = size(X,1);
% nu = size(u,1);
kf = size(u,2);

Init_Val = reshape(X,[nx*kf 1]);
% Init_Val = X(1:end-nx);

options = odeset('RelTol',1e-6,'AbsTol',1e-6);
[T,Y] = ode45(@StateEq,[t0 tf],Init_Val,options);
X_prop = reshape(Y(end,:),[nx kf]);

    function Xprime_dot = StateEq(t,X)

       Fvec = zeros(nx*kf,1);
       for i = 1:kf
           rangeX = nx*(i-1)+1:nx*i;
           f = eval_f(X(rangeX));
           g = eval_g();
           Fvec(rangeX) = f+g*u(:,i);
       end

       Xprime_dot = Fvec;
    end
end